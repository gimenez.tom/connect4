# Connect4

Connect4 on GameBoy.

![Connect4 main menu](main_menu.png) ![Connect4 player 1 wins](game_player_1_wins.png)

## Play
- Download ROM: connect4.gb
- Load the ROM in your favorite GameBoy emulator!

## Build
### Setup
- Install SDCC and add it to your PATH
- Compile (or get a compiled version of) GBDK-n
- Move all GBDK-n folder inside this project's folder
  - For instance, `gbdk-n-compile.sh` should be at path `connect4/gbdk-n/gbdk-n-compile.sh`

### Compile
- From your favorite terminal, go inside this project's folder
- Type `make`
